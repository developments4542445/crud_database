.. My_project documentation master file, created by
   sphinx-quickstart on Tue Feb 21 18:08:14 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to My_project's documentation!
======================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   controlador
   exceptions
   log_file
   modelo
   vista


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
