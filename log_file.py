import logging
import datetime


# Decorators
def my_log(*args):
    def _my_log(my_function):
        logging.basicConfig(filename='my_log_file.log', level='DEBUG')

        logging.info(str(datetime.datetime.now()) + "\t" + str(my_function.__name__) + "\t" + str(args[0]))

        return my_function
    return _my_log
